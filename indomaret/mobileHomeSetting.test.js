import { Selector } from 'testcafe'

fixture`New Mobile Homepage Setting`
    .page('http://172.18.186.86:9003/')

test.before(async t => {
    //selector
    const usernameInput = Selector('input[name="username"]')
    const passwordInput = Selector('input[name="password"]')
    const submitButton = Selector('button[type="submit"]')
    //action
    await t.typeText(usernameInput, 'devtest44@idm-app.id', { paste: true })
    await t.typeText(passwordInput, 'Edts2021!', { paste: true })
    await t.click(submitButton)
})
    ('User can create new module to the list', async t => {
        //selector
        const promotionsNavbar = Selector('a[data-key="promotions"]')
        const mobileHomepageSettingMenu = Selector('a[href="/mobile-homepage-setting"]')
        const createModuleButton = Selector('a').withText('Create Module')
        const radioSlidingPromoBanner = Selector('input[name="homepageTypeCode"]')
        const titleInput = Selector('input[name="title"]')
        const subtitleInput = Selector('input[name="subTitle"]')
        const isActiveInput = Selector('label').withText('Module Inactive')
        const addPromoButton = Selector('button').withText('Add Promo')
        const checkPromoId = Selector('input[value="396"]')
        const addPromoListButton = Selector('button').withText('Cancel').nextSibling('button').withText('Add Promo')
        const submitButton = Selector('button[type="submit"]').withText('Create Module')
        const confirmButton = Selector('button').withText('Confirm')
        //action
        await t.click(promotionsNavbar)
        await t.click(mobileHomepageSettingMenu)
        await t.click(createModuleButton)
        await t.click(radioSlidingPromoBanner)
        await t.typeText(titleInput, 'Title testcafe')
        await t.typeText(subtitleInput, 'Subtitle testcafe')
        await t.click(isActiveInput)
        await t.click(addPromoButton)
        await t.click(checkPromoId)
        await t.click(addPromoListButton)
        await t.click(submitButton)
        await t.click(confirmButton)
        //assertion
    })