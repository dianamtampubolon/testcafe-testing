import loginpage from '../pages/loginPage'
import promopage from '../pages/promoPage'

const dataSet = require('../data/data.json')

fixture`New Promo Test`
    .page('http://172.18.186.86:9003/')

dataSet.forEach(data => {
    test.before(async t => {
        //action
        await t.typeText(loginpage.usernameInput, 'devtest44@idm-app.id', { paste: true })
        await t.typeText(loginpage.passwordInput, 'Edts2021!', { paste: true })
        await t.click(loginpage.submitButton)
    })
        ('User can add new promo to the list', async t => {
            //action
            await t.click(promopage.promotionsNavbar)
            await t.click(promopage.promoMenu)
            await t.click(promopage.createPromoButton)
            await t.typeText(promopage.promoTitleInput, data.promoTitle, { paste: true })
            await t.typeText(promopage.promoSubTitleInput, data.promoSubtitle, { paste: true })
            await t.click(promopage.validStartDateInput)
            await t.expect(promopage.datePickStartDate.exists).ok()
            await t.click(promopage.datePickStartDate.withText("30"))
            await t.click(promopage.validEndDateInput)
            await t.expect(promopage.datePickEndDate.exists).ok()
            await t.click(promopage.datePickEndDate.withText("31"))
            // await t.click(categoryInput).filter('[label="Minuman"]')
            await t.typeText(promopage.descriptionInput, 'test promo desc testcafe')
            await t.click(promopage.modalUpload)
            await t.expect(promopage.fileUpload.exists).ok()
            await t.setFilesToUpload(promopage.fileUpload, './banner.jpeg')
            await t.click(promopage.fileUploadConfirm)
            await t.click(promopage.onBannerTap)
            await t.click(promopage.useCta)
            await t.click(promopage.submitButton)
            await t.expect(promopage.preview.withText('Please make sure that all information').exists).ok()
            await t.click(promopage.submitPreview)
            await t.expect(promopage.confirmCreatePromo.exists).ok()
            // await t.click(promopage.submitConfirmCreatePromo)

            //assertion
            // await t.expect(promopage.successMessage.exists).ok()
        })
})