
import { Selector } from "testcafe";

class LoginPage {
  constructor() {
    this.usernameInput = Selector('input[name="username"]')
    this.passwordInput = Selector('input[name="password"]')
    this.submitButton = Selector('button[type="submit"]')
  }
}

export default new LoginPage()