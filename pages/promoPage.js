import { Selector, t } from "testcafe";

class PromoPage {
    constructor() {
        this.promotionsNavbar = Selector('a[data-key="promotions"]')
        this.promoMenu = Selector('a[href="/promo-master"]')
        this.createPromoButton = Selector('a').withText('Create Promo')
        this.promoTitleInput = Selector('input[name="title"]')
        this.promoSubTitleInput = Selector('input[name="subTitle"]')
        this.validStartDateInput = Selector('input[name="startDate"]')
        this.datePickStartDate = Selector('.react-datepicker__month > .react-datepicker__week:nth-child(5) > .react-datepicker__day')
        this.validEndDateInput = Selector('input[name="endDate"]')
        this.datePickEndDate = Selector('.react-datepicker__month > .react-datepicker__week:nth-child(5) > .react-datepicker__day')
        this.categoryInput = Selector('.select-option-group option')
        this.descriptionInput = Selector('textarea[name="description"]')
        this.modalUpload = Selector('button').withText('Upload')
        this.fileUpload = Selector('input#image')
        this.fileUploadConfirm = Selector('.sc-jTzLTM.kPWwsU').withText('Submit')
        this.onBannerTap = Selector('input#radio0')
        this.useCta = Selector('input#onBannerDetail0')
        this.submitButton = Selector('button[type="submit"]')
        this.preview = Selector('p')
        this.submitPreview = Selector('button').withText('Submit')
        this.confirmCreatePromo = Selector('h3').withText('Create New Promo?')
        this.submitConfirmCreatePromo = Selector('button.sc-jTzLTM.jmZxoQ')
        this.successMessage = Selector('p').withText('Create Promo Master Successfully')
    }
}
export default new PromoPage();