import { Selector } from 'testcafe'

fixture("First Fixture with TestCafe")
    .page("https://devexpress.github.io/testcafe/example/");
test("First Test with TestCafe", async t => {
    //selector
    const inputUsername = Selector('#developer-name')
    const buttonSubmit = Selector('#submit-button')
    await t
        .typeText(inputUsername, 'Diana')
        // .click('input#macos')
        .click(buttonSubmit);
});

test.page("https://devexpress.github.io/testcafe/blog/", async t => {
    await t
    //     .typeText('#developer-name', 'Diana')
    //     .click('input#macos')
    //     .click('#submit-button');
});