import { Selector } from 'testcafe'

const developerName = Selector('#developer-name');
const osOption = Selector('input#macos')
const submitButton = Selector('#submit-button')

fixture("First Fixture with TestCafe")
    .page("https://devexpress.github.io/testcafe/example/");
test("First Test with TestCafe", async t => {
    await t
        .typeText(developerName, 'Diana')
        .click(osOption)
        .click(submitButton);
});