import { Selector } from 'testcafe'

fixture`New payee test`
    .page(`http://zero.webappsecurity.com/index.html`)

test.skip.before(async t => {
    const signInButton = Selector('#signin_button')
    const usernameInput = Selector('#user_login')
    const passwordInput = Selector('#user_password')
    const submitButton = Selector('.btn-primary')

    await t.click(signInButton)
    await t.typeText(usernameInput, 'username', { paste: true })
    await t.typeText(passwordInput, 'password', { paste: true })
    await t.click(submitButton)
})
    ('User can add new payee to the list', async t => {
        //selector
        const payeeBillsTab = Selector('#pay_bills_tab')
        const addNewPayeeTab = Selector('a').withText('Add New Payee')
        const inputName = Selector('#np_new_payee_name')
        const inputAddress = Selector('#np_new_payee_address')
        const inputAccount = Selector('#np_new_payee_account')
        const inputDetail = Selector('#np_new_payee_details')
        const buttonAdd = Selector('#add_new_payee')
        const message = Selector('#alert_content').innerText
        //action
        await t.click(payeeBillsTab)
        await t.click(addNewPayeeTab)
        await t.typeText(inputName, 'test name', { paste: true })
        await t.typeText(inputAddress, 'test address', { paste: true })
        await t.typeText(inputAccount, 'test account', { paste: true })
        await t.typeText(inputDetail, 'test detail', { paste: true })
        await t.click(buttonAdd)
        //assertion
        await t.expect(message).contains('successfully created.')
    })


